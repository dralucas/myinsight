# References Material

This directory contains some proceedings and papers about the InSight Mission



[-] A. Lucas et al., **InSight for seismically detectability and seismically triggered avalanches on Mars**, Europlanet Science Congress 2022, Granada, Spain, 18–23 Sep 2022, EPSC2022-366, (2022) [PDF](EPSC2022-366-print.pdf) 


[-] A. Lucas et al., **Investigation On Putative Explanation For Seis/Insight Unknown Events From Rock Avalanches And Rockfalls And Comparison With Alpine Cases**, *51st Lunar and Planetary Science Conference* (2020) [PDF](1840.pdf)

[-] A. Lucas et al., **Seismic Detection of Mass Wasting on Mars with SEIS/InSight: A Loony Attempt?**, *50th Lunar and Planetary Science Conference* (2019)  [PDF](2441.pdf)
