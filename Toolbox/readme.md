# This MyInSight Toolbox

## Mapping

 To map the ellipse of uncertainty for a given event, one can use a probabilistic approach. 
 
 This is done in 
 
  - a MatLab(c) code [MarsQuake_ProbLoc](MarsQuake_ProbLoc.m)

  - a Python Notebook [MarsQuake_EllipsProb](MarsQuake_EllipsProb.ipynb)



