%% MarsQuake probability location
% This script takes some estimate of bearing and distances from the SEIS
% station and then compute randomly the location accounting for
% uncertainties
%
% 
% Version 1.0, 15-09-2022
% Author: A. Lucas <lucas@ipgp.fr>
% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~`

%% Preambule
clc
close all hidden
clear all
disp(' MarsQuake probability location ')
disp(' ')
%% SEIS instrument location
st_long = 135.619641;
st_lat = 4.497476;
%% n_run
n_run = 1000; % number of runs, adjust
%% Data from Melanie's paper (https://doi.org/10.1029/2021JE007067)
%  Description of the file:
%  Name, offset,offset_err,depth,depth_err,lat,long,BAz,BAz_err

MEQ = readtable('MEQ.txt');

%% Load the figure
figure;clf;
hold on
plot(st_long,st_lat,'+r')

cmap = jet(size(MEQ,1));


%% Loop over the events from MEQ file
for kkkkk=1:size(MEQ,1)
    
    evk = kkkkk; % index for each event

    disp(['Treating Event',string(table2cell(MEQ(evk,1)))])

    lo2 = zeros(n_run,1);
    la2 = lo2;
    la1 = st_lat;
    lo1 = st_long;

    for k=1:n_run

        %read the data from the evk event
        Ad = random('Normal',table2array(MEQ(evk,2)),table2array(MEQ(evk,3))/2);
        rho = random('Normal',table2array(MEQ(evk,8)),table2array(MEQ(evk,9))/2);

        %location of a point from an initial location (i.e. SEIS) and a
        %bearing/distance tuple

        la2(k) =  asind(sind(la1) * cosd(Ad)  + cosd(la1) * sind(Ad) * cosd(rho));
        lo2(k) = lo1 + atan2d(sind(rho) * sind(Ad) * cosd(la1) , cosd(Ad) - sind(la1) * sind(la2(k)));


    end

    %Make the plot with different color
    plot(lo2,la2,'.','Color',cmap(kkkkk,:))

    axis equal tight
    axis xy


end

%Add some text/annotations

set(gcf,'Color','white')
xlabel('Longitude')
ylabel('Latitude')
text(st_long,st_lat,'SEIS/InsIght')

for k=1:size(MEQ,1)
    lo = table2array(MEQ(k,7));
    la = table2array(MEQ(k,6));
    meqname = string(table2cell(MEQ(k,1)));
    text(lo,la,meqname,'FontSize',16)

end